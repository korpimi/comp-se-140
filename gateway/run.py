import pika
import requests
from datetime import datetime
import logging
from http.server import BaseHTTPRequestHandler, HTTPServer

httpserv_url = "http://httpserv:8080"

class S(BaseHTTPRequestHandler):
    def _set_response(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/plain')
        self.end_headers()

    def do_GET(self):
        print(self.path)
        #Get messages from httpserv
        if self.path == "/messages":
            self._set_response()
            self.end_headers()
            r = requests.get(httpserv_url)
            print(r.text)
            self.wfile.write(r.text.encode('utf-8'))
        # Get logs from httpserv
        elif self.path == "/run-log":
            self._set_response()
            self.end_headers()
            r = requests.get(httpserv_url + '/logs')
            print(r.text)
            self.wfile.write(r.text.encode('utf-8'))
        # Get state from httpserv
        elif self.path == "/state":
            r = requests.get(httpserv_url + "/state")
            print(r.text)
            self._set_response()
            self.end_headers()
            self.wfile.write(r.text.encode('utf-8'))

    def do_PUT(self):
        print(self.path)
        if self.path == "/state": # <-- post_data includes some whitespaces and extra rows so this is done like this
            content_length = int(self.headers['Content-Length'])
            post_data = self.rfile.read(content_length).decode()
            if "INIT" in post_data:
                r = requests.post(httpserv_url + "/state", data = "INIT")
                if r.status_code != 200:
                    self.send_response(r.status_code)
                    return
                self.send_response(200)
                self.send_header('Content-type', 'text/plain')
                self.end_headers()
                self.wfile.write(post_data.encode('utf-8'))
                return
            elif "RUNNING" in post_data: 
                r = requests.post(httpserv_url + "/state", data = "RUNNING")
                if r.status_code != 200:
                    self.send_response(r.status_code)
                    return
                self.send_response(200)
                self.send_header('Content-type', 'text/plain')
                self.end_headers()
                self.wfile.write(post_data.encode('utf-8'))
                return
            elif "PAUSED" in post_data:
                state = post_data
                r = requests.post(httpserv_url + "/state", data = "PAUSED")
                if r.status_code != 200:
                    self.send_response(r.status_code)
                    return
                self.send_response(200)
                self.send_header('Content-type', 'text/plain')
                self.end_headers()
                self.wfile.write(post_data.encode('utf-8'))
                return
            elif "SHUTDOWN" in post_data:
                state = post_data
                r = requests.post(httpserv_url + "/state", data = "SHUTDOWN")
                if r.status_code != 200:
                    self.send_response(r.status_code)
                    return
                self.send_response(200)
                self.send_header('Content-type', 'text/plain')
                self.end_headers()
                self.wfile.write(post_data.encode('utf-8'))

        self.send_response(400)

# Start the http server
def run(server_class=HTTPServer, handler_class=S, port=80):
    logging.basicConfig(level=logging.INFO)
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    logging.info('Starting httpd...\n')
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    logging.info('Stopping httpd...\n')

if __name__ == '__main__':
    from sys import argv

    run()


