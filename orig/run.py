#!/usr/bin/env python
import pika
import sys
import time
import requests


def main():
    message_number = 0

    while True:
        # I ended up storing the state in the httpserv because of my simple
        # python implementation. Having multiple processes running by the
        # program would have taken a lot more time so this was the way I got
        # around it. The state could have been stored in a volume as well.
        # I tried to do it with rabbitmq, but was not able to get this looping code
        # working with it.
        r = requests.get("http://httpserv:8080/state")
        if "RUNNING" in r.text:
            connection = pika.BlockingConnection(pika.ConnectionParameters(host='rabbitmq'))
            channel = connection.channel()

            channel.exchange_declare(exchange='my.o', exchange_type='fanout')

            message_number += 1

            message = "MSG_{}".format(message_number)
            channel.basic_publish(exchange='my.o', routing_key='', body=message)
        time.sleep(3)

if __name__ == '__main__':
    main()