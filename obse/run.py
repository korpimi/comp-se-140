#!/usr/bin/env python
import pika
import requests
from datetime import datetime
import logging


http_server_url = 'http://httpserv:8080'


def main():
    connection = pika.BlockingConnection(
        pika.ConnectionParameters(host='rabbitmq'))
    channel1 = connection.channel()
    channel2 = connection.channel()

    channel1.exchange_declare(exchange='my.o', exchange_type='fanout')
    channel2.exchange_declare(exchange='my.i', exchange_type='fanout')

    result1 = channel1.queue_declare(queue='', exclusive=True)
    result2 = channel2.queue_declare(queue='', exclusive=True)

    channel1.queue_bind(exchange='my.o', queue=result1.method.queue)
    channel2.queue_bind(exchange='my.i', queue=result2.method.queue)

    def callback(ch, method, properties, body):
        timestamp = datetime.utcnow().isoformat()[:-3]+'Z'
        topic = method.exchange

        
        # send data to httpserv and save it to results.txt file there.
        # I ended up with using http to send data here instead of volumes because
        # I had a lot of issues after issues while trying to get volumes working here.
        # This was the way I ended up and didn't have time to resolve those issues.
        r = requests.post(http_server_url, data = "{} Topic {}: {} \n".format(timestamp, topic, body.decode()))

    channel1.basic_consume(
        queue=result1.method.queue, on_message_callback=callback, auto_ack=True)
    channel2.basic_consume(
        queue=result2.method.queue, on_message_callback=callback, auto_ack=True)

    channel1.start_consuming()
    channel2.start_consuming()


if __name__ == '__main__':
    main()