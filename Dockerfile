FROM python:3
ENV PYTHONUNBUFFERED=1
RUN mkdir /code
WORKDIR /code
COPY ./requirements.txt /code/requirements.txt
COPY ./wait-for-it.sh /wait-for-it.sh
RUN pip install -r requirements.txt
CMD [ "python", "run.py"]