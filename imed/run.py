#!/usr/bin/env python
import pika
import time

def main():
    #Setting up the connection
    connection = pika.BlockingConnection(pika.ConnectionParameters(host='rabbitmq'))
    channel = connection.channel()
    channel.exchange_declare(exchange='my.o', exchange_type='fanout')
    result = channel.queue_declare(queue='', exclusive=True)
    queue_name = result.method.queue
    channel.queue_bind(exchange='my.o', queue=queue_name)

    # Function to call when a message is received from my.o
    def callback(ch, method, properties, body):
        time.sleep(1)
        channel.exchange_declare(exchange='my.i', exchange_type='fanout')
        channel.basic_publish(exchange='my.i', routing_key='', body="Got {}".format(body.decode()))

    channel.basic_consume(
        queue=queue_name, on_message_callback=callback, auto_ack=True)
    channel.start_consuming()


if __name__ == '__main__':
    main()