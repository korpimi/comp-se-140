# Comp Se 140

My implementation is runnin on aws ec2 intance
it can be accessed from ip 13.49.134.147

The requests can be made from following endpoints
```
GET http://13.49.134.147/messages
GET/PUT http://13.49.134.147/state
GET http://13.49.134.147/run-log
GET http://13.49.134.147/shutdown
```

If you were to run the program locally you would have to build the program using the following commands:
```
docker build . -t compse
docker-compose up -d --force-recreate
```

the gateway runs on port localhost:80


## Testing
For testing I used postman tests. I generated a json file which has postman requests and tests included from them. They test that the response codes are right and the responses contain things. Tests are fairly simple. 

The application is first deployed in the ci file to the environment and the tests are run afterwards using rest endpoints as end-to-end tests.

There probably could be smoke testing before using python unit tests but the rabbitmq servers would be hard to test with using unit tests and if I did unit test them, they would mostly be done using mocking which don't bring too much value.