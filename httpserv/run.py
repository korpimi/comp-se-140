from http.server import BaseHTTPRequestHandler, HTTPServer
import logging
import fileinput
from datetime import datetime

state = "RUNNING"

class S(BaseHTTPRequestHandler):
    def _set_response(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/plain')
        self.end_headers()

    def do_GET(self):
        print(self.path)
        # Get current state.
        if self.path == "/state":
            self._set_response()
            self.end_headers()
            self.wfile.write(state.encode('utf-8'))

        # Return rows from logs.txt file
        elif self.path == "/logs":
            try:
                self._set_response()
                self.end_headers()
                f = open('logs.txt', 'rb') 
                self.wfile.write(f.read())
            except IOError:
                print("File not accessible")
                self.set_response(404)
            finally:
                f.close()
                
        # Return results.txt file rows
        elif self.path == "/":
            try:
                self._set_response()
                self.end_headers()
                f = open('results.txt', 'rb') 
                self.wfile.write(f.read())
            except IOError:
                print("File not accessible")
                self.set_response(404)
            finally:
                f.close()
        else:
            self.send_response(404)


    def do_POST(self):
        # Set state
        if self.path == "/state":
            content_length = int(self.headers['Content-Length']) # <--- Gets the size of data
            post_data = self.rfile.read(content_length).decode()
            timestamp = datetime.utcnow().isoformat()[:-3]+'Z: '
            global state
            if "INIT" in post_data:
                state = "RUNNING"
                f = open('results.txt', 'w').close()
                f = open('logs.txt', "a")
                f.write(timestamp + "INIT\n")
                f.close()
                self._set_response()
                self.wfile.write("New state {}".format(state).encode('utf-8'))
                return
            elif "RUNNING" in post_data: 
                state = "RUNNING"
                f = open('logs.txt', "a")
                f.write(timestamp + "RUNNING\n")
                f.close()
                self._set_response()
                self.wfile.write("New state {}".format(state).encode('utf-8'))
                return
            elif "PAUSED" in post_data:
                state = "PAUSED"
                f = open('logs.txt', "a")
                f.write(timestamp + "PAUSED\n")
                f.close()
                self._set_response()
                self.wfile.write("New state {}".format(state).encode('utf-8'))
                return
            elif "SHUTDOWN" in post_data:
                raise KeyboardInterrupt

        # receive data from obse through http and save it to results.txt file.
        # I ended up with using http to send data here instead of volumes because
        # I had a lot of issues after issues while trying to get volumes working here.
        # This was the way I ended up and didn't have time to resolve those issues.
        elif self.path == "/":
            content_length = int(self.headers['Content-Length']) # <--- Gets the size of data
            post_data = self.rfile.read(content_length)
            f = open("results.txt", "ab")
            f.write(post_data)
            f.close()
            self._set_response()
            self.wfile.write("POST request for {}".format(self.path).encode('utf-8'))
        else:
            self.send_response(404)


# Start http server
def run(server_class=HTTPServer, handler_class=S, port=8080):
    logging.basicConfig(level=logging.INFO)
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    open('results.txt', 'w').close()
    open('logs.txt', 'w').close()
    logging.info('Starting httpd...\n')
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    logging.info('Stopping httpd...\n')

if __name__ == '__main__':
    from sys import argv

    run()


